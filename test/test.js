var supertest = require("supertest");
var should = require("should");
var server = supertest.agent("http://localhost:3000");

describe("User",function(){
    it("user index",function(done){
        server
            .get("/user/")
            .expect("Content-type",/json/)
            .expect(200)
            .end(function(err,res){
                res.status.should.equal(200);
                res.body.s.should.equal(1);
                done();
            });
    });
});

describe("User track",function(){
    it("user track index",function(done){
        server
            .get("/usertrack/")
            .expect("Content-type",/json/)
            .expect(200)
            .end(function(err,res){
                res.status.should.equal(200);
                res.body.s.should.equal(1);
                done();
            });
    });
});