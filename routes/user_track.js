const router = require('express').Router();
const wrap = require('co-express');

/**
 * @swagger
 * resourcePath: /usertrack
 * description: User track CRUD
 */


router.get('/', wrap(function *(req, res) {
    res.json({s: 1});
}));

module.exports = router;
